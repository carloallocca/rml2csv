/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.hcmr.imbbc.rmlreverse.project.strategy;

/**
 *
 * @author callocca
 */
public interface ReverseRMLStrategy {

    /**
     *
     * @param path2RMLFile
     * @param path2RDFDataset
     * @param path2CSVDataSource
     */
    public void reverseRML(String path2RMLFile, String path2RDFDataset, String path2CSVDataSource);
    public void reverseRML();
}
