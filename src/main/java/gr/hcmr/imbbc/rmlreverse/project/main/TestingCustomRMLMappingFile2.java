/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.hcmr.imbbc.rmlreverse.project.main;

import be.ugent.mmlab.rml.model.TriplesMap;
import gr.hcmr.imbbc.rmlreverse.project.tools.CustomRMLMappingFile;
import java.util.ArrayList;

/**
 *
 * @author callocca
 */
public class TestingCustomRMLMappingFile2 {
    
     public static void main(String[] args){
        
        System.out.println("Start");
        
        //running correctly
        //String path2RMLFile ="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\Code RML Reverse 01 04 2015\\InputDataset\\dataset7\\rml7.ttl";
        //running correctly
        String path2RMLFile ="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\Code RML Reverse 01 04 2015\\InputDataset\\dataset7\\rml7_1.ttl";
       
        //running correctly
        //String path2RMLFile ="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\Code RML Reverse 01 04 2015\\InputDataset\\dataset7\\rml7_2.ttl";
        
        //running correctly
        //String path2RMLFile ="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\Code RML Reverse 01 04 2015\\InputDataset\\dataset7\\rml7_3.ttl";
        
        //running correctly
        //String path2RMLFile ="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLProcessor\\ReverseRMLInput\\RMLmappingFileForBuildingForest.rml.ttl";        
        String path2reverseRMLFile="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\Code RML Reverse 01 04 2015\\InputDataset\\dataset7\\reverseRML.properties";
        
        CustomRMLMappingFile rmlMappingFile= new CustomRMLMappingFile(path2reverseRMLFile);
        
//        boolean isStructuralEnriched=rmlMappingFile.isStructuralEnriched(path2RMLFile);
        boolean isStructuralEnriched=rmlMappingFile.isStructuralEnriched(path2RMLFile);
        
        if(isStructuralEnriched){
            System.out.println("[TestingCustomRMLMappingFile2:Main] The RML Mapping File is structural enriched ");
        }
        else{
            System.out.println("[TestingCustomRMLMappingFile2:Main] The RML Mapping File is not structural enriched");
        }
        System.out.println("End");
        
        
        
    }
    
}
