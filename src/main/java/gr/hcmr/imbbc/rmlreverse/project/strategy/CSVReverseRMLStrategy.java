/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.hcmr.imbbc.rmlreverse.project.strategy;

import be.ugent.mmlab.rml.core.RMLMappingFactory;
import be.ugent.mmlab.rml.model.PredicateObjectMap;
import be.ugent.mmlab.rml.model.RMLMapping;
import be.ugent.mmlab.rml.model.SubjectMap;
import be.ugent.mmlab.rml.model.ObjectMap;
import be.ugent.mmlab.rml.model.PredicateMap;
import be.ugent.mmlab.rml.model.ReferencingObjectMap;
import be.ugent.mmlab.rml.model.TriplesMap;
import gr.hcmr.imbbc.rmlreverse.project.tools.CustomRMLMappingFile;
import gr.hcmr.imbbc.rmlreverse.project.tools.NT_RDFDataset;
import gr.hcmr.imbbc.rmlreverse.project.tools.RDFDataset;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import org.openrdf.model.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.antidot.semantic.rdf.rdb2rdf.r2rml.exception.InvalidR2RMLStructureException;
import net.antidot.semantic.rdf.rdb2rdf.r2rml.exception.InvalidR2RMLSyntaxException;
import net.antidot.semantic.rdf.rdb2rdf.r2rml.exception.R2RMLDataError;
import org.apache.commons.lang.StringUtils;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFParseException;

/**
 *
 * @author callocca
 */
public class CSVReverseRMLStrategy implements ReverseRMLStrategy{
    
    //private HashMap<Integer, ArrayList<String>> CSVDataSource = new HashMap<Integer, ArrayList<String>>();
    // The output of the reversing procedure
    // (the first line is comprised by the header, e.g the names of the columns)
    private ArrayList<HashMap<String,String>> CSVDataSource = new ArrayList<HashMap<String,String>>();
    
    
    
    private String pathToRMLFile;
    private String pathToRDFDataset;
    private String pathToCSVDataSource;
    
    private TriplesMap rootTripleMap;
    private Collection<TriplesMap> triplesMapCollection;
    
    private boolean isRMLMappingFileTree=false;
    
    private CustomRMLMappingFile custRMLmappFile;
    private String tdbDir;
    
    
    private TriplesMap searchNodeByName(String mapName,Collection<TriplesMap> tree){
        TriplesMap result = null;
        for(TriplesMap node : tree){
            if(mapName.equals(node.getName())){
                result = node;
            }
        }
        return result;
    }
    
    public CSVReverseRMLStrategy(){
        super();
    }
    
    public CSVReverseRMLStrategy(String path2RMLFile, String path2RDFDataset, String path2CSVDataSource, String tdb_Dir){
        this.pathToRMLFile=path2RMLFile;
        this.pathToRDFDataset=path2RDFDataset;
        this.pathToCSVDataSource=path2CSVDataSource;
        this.tdbDir=tdb_Dir;
        
        this.custRMLmappFile= new CustomRMLMappingFile();
        if(custRMLmappFile.isRMLMappingFileTree(pathToRMLFile)){
            isRMLMappingFileTree=true;
            rootTripleMap = custRMLmappFile.identifyTheRootTripleMap(pathToRMLFile);
            triplesMapCollection = (Collection<TriplesMap>) custRMLmappFile.getTriplesMapCollection(pathToRMLFile);//.getTriplesMaps();
            System.out.println("[CSVReverseRMLStrategy] The tree condition is satisfied...");
        }
        else{
            System.out.println("[CSVReverseRMLStrategy] The necessary condition is not satisfied...");
        }
    }
    

    @Override
    public void reverseRML(String path2RMLFile, String path2RDFDataset, String path2DataSource) {
        System.out.println("[CSVReverseRMLStrategy:reverseRML] Please, we are working on it ...");
    }
    @Override
    public void reverseRML() {
        System.out.println("[CSVReverseRMLStrategy:reverseRML] Please, we are working on building the header of the CSVDataset");                
        
        //this is just for debugging
        //it list the name of the all triple maps that were found. 
        echo("List of triple maps:","blue");
        for(TriplesMap map : triplesMapCollection){
            echo(map.getName());
        }
        echo("");
        
        //1. Building the header of the CSVDataSource;
        buildingCSVDataSourceHeader();       
        echo("The Header of the CSV:","blue");
        printCSVDataSource();
        
        //2. Getting the root tripleMap
        TriplesMap rootTripleMap= custRMLmappFile.identifyTheRootTripleMap(pathToRMLFile);
        echo("Root triple map name: "+rootTripleMap.getName(),"blue");
        echo("");
        
        //3. Preparation for Step 4: ListOfDistinctSubjects <== Select the distinct subjects using the dT root node and the rdfD;
        //So far we addressed that the Set<URI> just contains one element. This is the assumption of our initial 
        //algorithm presented in the paper submitted at ESWC workshop. 
        Set<org.openrdf.model.URI> classesURIs = rootTripleMap.getSubjectMap().getClassIRIs();
        
        
        //it takes the only one element present in the set. Actually it is alway the last one. 
        String class_URI="";
        for(org.openrdf.model.URI classURI:classesURIs){
            class_URI= classURI.toString();            
        }
        
        
        if(!(class_URI=="")){
            // Preparing the RDF dataset as a model to be queried, in order to extract the "distinctSubjects"
            // Get all the triples that belong to the root node
            RDFDataset rdfDataset = new NT_RDFDataset(pathToRDFDataset, tdbDir);
            
            //Extract the distinctt subjects
            //4. Retrieving the distinct subjects
            ArrayList<String> subjectList=rdfDataset.selectDistinctSubject(class_URI);
            
            echo("Root node distinct subjects:","blue");
            echo(subjectList);
            echo("");
            
            if(!(subjectList==null) && (subjectList.size()>0)){
                // 5. For each distinct subject one CSV row will be created
                for (String subj  : subjectList) {                                        
                    
                    //We use an hashMap as the structure to record a row. It has structured as 
                    //<columName, columnValue> both as string
                    HashMap<String,String> row = new HashMap<String,String>();
                    String currentPredicate = "";
                    row = ReverseNextRow(subj,row,currentPredicate,rootTripleMap,pathToRDFDataset,triplesMapCollection,0,rdfDataset);                                        
                    //Step 5.2:  CSVDataSource.add(row(i));
                    CSVDataSource.add(row);
                }//endFOR
            }//endIF
        }//endIF
        
//        printCSVDataSource();
        saveCSVDataSource();
    }
    
    private HashMap<String,String> ReverseNextRow(String subjI,HashMap<String,String> row,String predicate,TriplesMap curTripleMap,String rdfPath,Collection<TriplesMap> tree,int depth,RDFDataset rdfDataset){
	
        echo(depth+1,"Reccursive method -- Depth: "+depth,"red");
        echo(depth+1,"subjI: " +subjI,"blue");
        echo(depth+1,"This map: "+curTripleMap.getName());
        
        // if the current node of the DT is null
        if(curTripleMap == null){
		return null;
	} else {
                //in case it is not null, then
		String nextSubj = subjI;		
		Set<PredicateObjectMap> poMapList = curTripleMap.getPredicateObjectMaps();
		// This is a leaf node or a root node without children
		if(poMapList.size() == 0){
			// It is a leaf node
			// We use the depth variable to simulate the following:
                        // if (POMList.size == 0) and (curreNode.Name != rootNode.Name)
                        if(depth > 0) { //we use the depth variable for just debugging purpose, to better understanding the level of recursion
				return row;
			} // It is a root node without children
			else {
                        // It means that
                        // if (POMList.size == 0) and (curreNode.Name == rootNode.Name)
                            
                                // We process the subjectTemplate for preparing the columnName
                                // attribues =[datasetID, eventDate]
                                ArrayList<String> attributes = new ArrayList<String>();
                                ArrayList<String> values = new ArrayList<String>();
                                //we get the subject template
				String subjectTemplate = curTripleMap.getSubjectMap().getStringTemplate();
                                
                                // Get the names of the columns that has been used in the
                                // triple subject
                                //select the last part of the subjectTemplate
                                int slashIndex=subjectTemplate.lastIndexOf('/');
                                String lastPart = subjectTemplate.substring(slashIndex+1,subjectTemplate.length());
                                
                                //we slipt the the last part according to the "_" carachter
                                for(String column : lastPart.split("_")){
                                    //taking the value inside the brakest
                                    String columnName = StringUtils.stripEnd(StringUtils.stripStart(column,"{"),"}");
                                    attributes.add(columnName);
                                }
				
                                // We process the indivudual - subjI - from the RDF dataset to fill the columnName with values
                                // values =[4, 15-sept-2011]
                                // Get the column values that has been used in the
                                // triple subject
                                slashIndex=subjI.lastIndexOf('/');
                                lastPart = subjI.substring(slashIndex+1,subjI.length());
                                for(String columnValue : lastPart.split("_")){                                    
                                    values.add(columnValue);
                                }
                                
                                // Add the column name/value pairs to the row
                                // We build the association between attribues =[datasetID, eventDate] and values =[4, 15-sept-2011]
                                // We will fill the hashMap row with such associations
				int counter = 0;
				for (String attributeName : attributes) {
                                        echo(depth+2,"Adding value: "+attributeName+"@"+values.get(counter));
					row.put(attributeName,values.get(counter));                                        
					counter++;
				}
				return row;
			}	
		} else { // Part B (root node with children or intermediate node)
                        // We are in the case of PredicateObjectMapList.size >0
                        // 2.4.1
			for(PredicateObjectMap poMap : poMapList){
				String currentPredicate = null;				
			//	ObjectMap oMap = null;
				String parentMapName = null;							
				
				// 2.4.1.C  We care only for the ones that include a referencing object map
				if(poMap.hasReferencingObjectMaps()){ 
					// ----- Retrieve the current predicate -----
					// We suppose that there is only one predicate //and it is a constant
					// the following code will work
					for(PredicateMap pMap : poMap.getPredicateMaps()){
						currentPredicate = pMap.getConstantValue().stringValue();
					}		
					// 
					
					// ----- Retrieve the object map -----
					// We supposed that there is only one ObjectMap
					// the following code will work
//					for(ObjectMap pMap : poMap.getObjectMaps()){
//						oMap = pMap;
//					}
//				
					// Retrieve the parent triple map name 
					Set<ReferencingObjectMap> roMapList =poMap.getReferencingObjectMaps(); 
					for(ReferencingObjectMap roMap:roMapList){ 
						parentMapName = roMap.getParentTriplesMap().getName();
					}
					
                                        echo(depth+1,"Parent map: "+parentMapName);
                                        
					// Retrieve the triple map with that name from the tree 
					TriplesMap nextNode = searchNodeByName(parentMapName,tree);                                        
                                        
                                        // 2.4.1.C.5
                                        // we start processing the template of the Next node for building the attribute name. 
					String nextSubjectTemplate = nextNode.getSubjectMap().getStringTemplate();
					Set<URI> classes  = nextNode.getSubjectMap().getClassIRIs();
                                        String className = "";
					for(URI aURI : classes){
						className = aURI.stringValue();  // or extract from URI (after the last # or /)
					}				
					
                                        // 2.4.1.C.4
                                        ArrayList<String> nextAttributes = new ArrayList<String>();
					int slashIndex=nextSubjectTemplate.lastIndexOf('/');
                                        String lastPart = nextSubjectTemplate.substring(slashIndex+1,nextSubjectTemplate.length());
                                        for(String column : lastPart.split("_")){                                            
                                            String columnName = StringUtils.stripEnd(StringUtils.stripStart(column,"{"),"}");
                                            nextAttributes.add(columnName);
                                        }
                                        
                                        // 2.4.1.C.6     
//                                        echo(depth+1,"Searching:");
//                                        echo(depth+2,"nextSubjectValue: "+subjI);
//                                        echo(depth+2,"currentPredicate: "+currentPredicate);
//                                        echo(depth+2,"class: "+className);
                                        
                                        // we start processing the values of the Next node subject  for building the attribute value. 
                                        ArrayList<String> nextSubjectValues = rdfDataset.selectDistinctObject(subjI, currentPredicate, className); // extract it from rdfData (it will be an IRI)
                                        
                                        //we assume that there is only one value as described in the ESWC paper
                                        String nextSubjectValue = nextSubjectValues.get(0);
                                        
                                        ArrayList<String> values = new ArrayList<String>();
                                        slashIndex=nextSubjectValue.lastIndexOf('/');
                                        lastPart = nextSubjectValue.substring(slashIndex+1,nextSubjectValue.length());
                                        for(String columnValue : lastPart.split("_")){                                    
                                            values.add(columnValue);
                                        }
                                        
                                        // 2.4.1.C8 and 2.4.1.C.9
                                        // we build the association between attributeName and AttributeValues
					int counter = 0;                                       
					for (String attributeName : nextAttributes) {
                                                echo(depth+2,"Adding value: "+attributeName+"@"+values.get(counter));
						row.put(attributeName,values.get(counter)); 
						counter++;
					}
                                        // we recorsively recall the main reverseRow
					row = ReverseNextRow(nextSubjectValue,row,currentPredicate,nextNode,pathToRDFDataset,triplesMapCollection,depth+1,rdfDataset);
					echo(depth+1,"Back to reccursive method -- Depth: "+depth,"red");
                                        // if the current node is the root node
					if(depth == 1) { 
						String subjectTemplate = curTripleMap.getSubjectMap().getStringTemplate();
                                                
                                                ArrayList<String> rootAttributes = new ArrayList<String>();
                                                slashIndex=subjectTemplate.lastIndexOf('/');
                                                lastPart = subjectTemplate.substring(slashIndex+1,subjectTemplate.length());
                                                for(String column : lastPart.split("_")){
                                                    String columnName = StringUtils.stripEnd(StringUtils.stripStart(column,"{"),"}");
                                                    rootAttributes.add(columnName);
                                                }
                                                
                                                ArrayList<String> rootValues = new ArrayList<String>();
                                                slashIndex=subjI.lastIndexOf('/');
                                                lastPart = subjI.substring(slashIndex+1,subjI.length());
                                                for(String columnValue : lastPart.split("_")){                                    
                                                    rootValues.add(columnValue);
                                                }
                                                
						counter = 0;
						for (String attributeName : rootAttributes) {
                                                        echo(depth+2,"Adding value: "+rootAttributes+"@"+rootValues.get(counter));
							row.put(attributeName,rootValues.get(counter));                                                        
							counter++;
						}
					}
					
				}										
			}
			return row;
		}
	}
}
    
    private void buildingCSVDataSourceHeader(){
        HashMap<String,String> CSVHeader=new HashMap<String,String>();
        for(TriplesMap triple_map:triplesMapCollection){
            //System.out.println("TripleMap: "+triple_map.getName());
            SubjectMap sub_map = triple_map.getSubjectMap();
            if(!(sub_map.getStringTemplate()==null)){                
                String stringTemplate = sub_map.getStringTemplate();
                //System.out.println("stringTemplate: "+stringTemplate);
                String localName = extractLocalNameFromStringTemplate(stringTemplate);
                
                if(localName.contains("_")){
                    String[]columns=localName.split("_");
                    for(int i=0; i<columns.length; i++){
                        CSVHeader.put(columns[i].substring(1,columns[i].length()-1),"");
                            
                    }
                }
                else{   
                    CSVHeader.put(localName.substring(1,localName.length()-1),"");                    
                }
                //System.out.println("LocalNameTemplate: "+localName);
                //CSVHeader.add(localName);
            }
            System.out.println(" ");
        }
        if(!CSVHeader.isEmpty()){
            CSVDataSource.add(CSVHeader);//.put(0, CSVHeader);
        }
    }
    
    private String extractLocalNameFromStringTemplate(String template){
        //System.out.println(template);
        String localName=template.substring(template.lastIndexOf("/")+1);
        return localName;   
    }
        
    private void printCSVDataSource(){
        for(HashMap<String,String> row:CSVDataSource){
            System.out.println(row.toString());
        }
    }
    
    private void saveCSVDataSource(){
        // Build the output file path
    //    File file = new File(this.pathToRDFDataset);
    //  File file = new File(this.pathToCSVDataSource);
  //      String outputPath = file.getParent()+"\\reversed.csv";
        //this.pathToCSVDataSource;
    //    String outputPath = file.getParent()+File.separatorChar+this.pathToCSVDataSource;//"\\reversed.csv";
        
        File targetFile = new File(this.pathToCSVDataSource);
        
        try {
            // Create the file if does not exist
            targetFile.createNewFile();
            // Prepare to write
            PrintWriter csv = new PrintWriter(targetFile);
            
            // Get a list of CSV column names
            // (we need to use the same column order when writing each CSV row
            HashMap headers = CSVDataSource.get(0);            
            String[] columnNames = new String[headers.size()];
            int sindex = 0;
            for (Object key : headers.keySet()) {
                columnNames[sindex] = key.toString();
                sindex++;
            }
            // Write the first line to CSV (the one with column names)
            csv.println(StringUtils.join(columnNames,","));
            
            int line = 0;
            // For each set of row values
            for(HashMap<String,String> row:CSVDataSource){                
                if(line > 0){
                    // Retrieve the values in the same order as the first line
                    String[] rowValues = new String[headers.size()];
                    int vindex = 0;
                    for(String cName: columnNames){
                        rowValues[vindex] = row.get(cName);
                        vindex++;
                    }
                    // Write values to CSV file
                    csv.println(StringUtils.join(rowValues,","));
                }
                line++;
            }
            csv.close();
        } catch (IOException ex) {
            Logger.getLogger(CSVReverseRMLStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    protected void echo(String message){
        System.out.println(message);
    }
    
    protected void echo(int indents, String message){
        String indent = "    ";
        for(int i=0; i<indents; i++){
            System.out.print(indent);
        }
        System.out.println(message);
    }
    
    protected void echo(String message, String color){
        String colorSeq = null;
                
        switch(color){
            case "red":
                colorSeq = "31m";
                break;
            case "blue":
                colorSeq = "34m";
                break;
            case "magenta":
                colorSeq = "35m";
                break;
            case "green":
                colorSeq = "32m";
                break;
            case "gray":
                colorSeq = "37m";
                break;
        }
        System.out.println((char)27 + "[" + colorSeq + message + (char)27 + "[37m");
    }
    
    protected void echo(ArrayList<String> list){
        for(String item : list){
            echo(item);
        }
    }
    
    protected void echo(ArrayList<String> list, String color){
        for(String item : list){
            echo(item,color);
        }
    }
    
    protected void echo(int indents, String message, String color){
        String indent = "    ";
        String colorSeq = null;
                
        for(int i=0; i<indents; i++){
            System.out.print(indent);
        }
        switch(color){
            case "red":
                colorSeq = "31m";
                break;
            case "blue":
                colorSeq = "34m";
                break;
            case "magenta":
                colorSeq = "35m";
                break;
            case "green":
                colorSeq = "32m";
                break;
            case "gray":
                colorSeq = "37m";
                break;
        }
        System.out.println((char)27 + "[" + colorSeq + message + (char)27 + "[37m");
    }
    
}
