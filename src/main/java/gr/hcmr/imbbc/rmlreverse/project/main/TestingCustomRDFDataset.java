/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.hcmr.imbbc.rmlreverse.project.main;

import com.hp.hpl.jena.query.QuerySolution;
import gr.hcmr.imbbc.rmlreverse.project.tools.CustomRDFDataset;
import gr.hcmr.imbbc.rmlreverse.project.tools.NT_RDFDataset;
import gr.hcmr.imbbc.rmlreverse.project.tools.Quad_RDFDataset;
import gr.hcmr.imbbc.rmlreverse.project.tools.RDFDataset;
import java.util.ArrayList;

/**
 *
 * @author callocca
 */
public class TestingCustomRDFDataset {
    
     public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("Start");
        
       
        // Testing an NT_RDFDataset
        
//        String rdfDatasetPath="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\tdbDirRMLReverse\\InputDataset\\data.nt";
//        String tdbDir="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\tdbDirRMLReverse\\TDB";
        
        String rdfDatasetPath ="/Users/carlo/Desktop/tdbDirRMLReverse/InputDataset/RDFDataset.nt";//C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLProcessor\\ReverseRMLInput\\RMLmappingFileForBuildingDependencyTree.rml.ttl";
        String tdbDir="/Users/carlo/Desktop/tdbDirRMLReverse/TDB";
        
        // quadModel(quadFilePath);
        RDFDataset rdfD=new NT_RDFDataset(rdfDatasetPath,tdbDir);
        
        String fullClassURI= "http://purl.org/NET/cidoc-crm/core/E21_Dataset";
        ArrayList<String> distingSubject= rdfD.selectDistinctSubject(fullClassURI);
        
        for(String subj:distingSubject){
            System.out.println(subj);
        }

//        String subjectURI ="http://lifeWatchGreece.gr/BC21/4";
//        String predicateURI= "http://purl.org/NET/cidoc-crm/core#P9_consist_of"; 
//        String rdfTypeClassURI= "http://purl.org/NET/cidoc-crm/core#E57_Sampling_Activity";
//        
//        ArrayList<String> distingObject= rdfD.selectDistinctObject(subjectURI, predicateURI, rdfTypeClassURI);//.selectDistinctSubject(fullClassURI);
//        for(String obj:distingObject){
//            System.out.println(obj);
//        }

        

//        String subjectURI ="http://lifeWatchGreece.gr/BC21/4";
//        String predicateURI= "http://purl.org/NET/cidoc-crm/core#P9_consist_of"; 
//        String rdfTypeClassURI= "http://purl.org/NET/cidoc-crm/core#E57_Sampling_Activity";
//
//        String queryString =        
//            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
//            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
//            "SELECT  ?object "+
//                "WHERE "
//                    +" {"
//                        +"<"+subjectURI+">"+ "<"+predicateURI+">"+ " ?object . "
//                        +"?object rdf:type"+ "<"+rdfTypeClassURI+"> ."
//                    + "}";
//
//        
//        ArrayList<QuerySolution> distingSol= rdfD.executeSelectQuery(queryString);//.selectDistinctObject(subjectURI, predicateURI, rdfTypeClassURI);//.selectDistinctSubject(fullClassURI);
//        for(QuerySolution sol:distingSol){
//            System.out.println(sol.get("object").toString());
//        }

        
//        // Testing an Quad_RDFDataset
//        
//        String rdfDatasetPath="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLReverseQuadModel\\Input\\rdf.nq";
//        String tdbDir="C:\\Users\\callocca\\Desktop\\tdbDirRMLReverse";        
//        RDFDataset rdfD=new Quad_RDFDataset(rdfDatasetPath,tdbDir);
//
//        
//        String fullClassURI= "http://xmlns.com/foaf/0.1#Person";
////        ArrayList<String> distingSubject= rdfD.selectDistinctSubject(fullClassURI);
////        
////        for(String subj:distingSubject){
////            System.out.println(subj);
////        }
////
//        
//        String subjectURI ="http://dataset1/alexandros+gougousis";
//        String predicateURI= "http://purl.org/NET/cidoc-crm/core#P9_consist_of"; 
//        String rdfTypeClassURI= "http://xmlns.com/foaf/0.1#Person";
//
//        String queryString =        
//            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
//            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
//            "SELECT  ?object "+
//                "WHERE "
//                    +" {"
//                        +"<"+subjectURI+">"+ "<"+predicateURI+">"+ " ?object . "
//                        +"?object rdf:type"+ "<"+rdfTypeClassURI+"> ."
//                    + "}";
//
//        
//        ArrayList<QuerySolution> distingSol= rdfD.executeSelectQuery(queryString);//.selectDistinctObject(subjectURI, predicateURI, rdfTypeClassURI);//.selectDistinctSubject(fullClassURI);
//        for(QuerySolution sol:distingSol){
//            //System.out.println(sol.get("object").toString());
//        }
//
//        
        
        
        System.out.println("End");
        
        
    }
    
}
