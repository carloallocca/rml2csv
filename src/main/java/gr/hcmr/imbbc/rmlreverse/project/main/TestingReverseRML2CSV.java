/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.hcmr.imbbc.rmlreverse.project.main;

import gr.hcmr.imbbc.rmlreverse.project.strategy.CSVReverseRMLStrategy;
import gr.hcmr.imbbc.rmlreverse.project.strategy.ReverseRML2CSV;
import gr.hcmr.imbbc.rmlreverse.project.strategy.ReverseRMLStrategy;

/**
 *
 * @author callocca
 */
public class TestingReverseRML2CSV {
    public static void main(String[] args){
        
        String prefix="C:\\Users\\callocca\\Desktop\\ReverseRMLBanchMark\\dataset";
        //String prefix="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\Code RML Reverse 01 04 2015\\InputDataset\\dataset";
        String dataset_id = "8";
        
        String path2RMLFile =prefix+dataset_id+"\\rml"+dataset_id+".ttl";
        String path2RDFDataset =prefix+dataset_id+"\\rdf"+dataset_id+".nt";
        String path2reverseRMLproperties =prefix+dataset_id+"\\reverseRML.properties";        
        String path2CSVDataSource = prefix+dataset_id+"\\reversedDataset"+dataset_id+".csv";        
        //String path2CSVDataSource = "reversedDataset"+dataset_id+".csv";        
        String tdbDir=prefix+dataset_id+"\\tmp";
    
        //ReverseRMLStrategy rmlMappingFile= new ReverseRML2CSV(path2RMLFile,path2RDFDataset,path2reverseRMLproperties,path2CSVDataSource, tdbDir);
        //ReverseRML2CSV rmlMappingFile= new ReverseRML2CSV(path2RMLFile,path2RDFDataset,path2reverseRMLproperties,path2CSVDataSource, tdbDir);
        //ReverseRML2CSV rmlMappingFile= new ReverseRML2CSV(path2RMLFile,path2RDFDataset,path2reverseRMLproperties,path2CSVDataSource, tdbDir);
        
        // TO DO
        //rmlMappingFile.reverseRML();
//        rmlMappingFile.reverseRMLNew();
        
        //This is working...this is the old implementation        
        //ReverseRMLStrategy rmlMappingFile= new CSVReverseRMLStrategy(path2RMLFile,path2RDFDataset,path2CSVDataSource,tdbDir);
        //rmlMappingFile.reverseRML();
        
        // Let's try the new implementation. 
        // It is also working. We continued to develop this class one instead of CSVReverseRMLStrategy.
        ReverseRMLStrategy rmlMappingFile= new ReverseRML2CSV(path2RMLFile,path2RDFDataset,path2CSVDataSource,tdbDir);
        rmlMappingFile.reverseRML();
        
        
        System.out.println("End");
    }
}
