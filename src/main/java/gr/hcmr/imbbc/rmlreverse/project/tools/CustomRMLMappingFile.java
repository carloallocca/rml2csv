/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.hcmr.imbbc.rmlreverse.project.tools;

import be.ugent.mmlab.rml.core.RMLMappingFactory;
import be.ugent.mmlab.rml.model.ObjectMap;
import be.ugent.mmlab.rml.model.PredicateMap;
import be.ugent.mmlab.rml.model.PredicateObjectMap;
import be.ugent.mmlab.rml.model.RMLMapping;
import be.ugent.mmlab.rml.model.ReferencingObjectMap;
import be.ugent.mmlab.rml.model.TriplesMap;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.antidot.semantic.rdf.rdb2rdf.r2rml.exception.InvalidR2RMLStructureException;
import net.antidot.semantic.rdf.rdb2rdf.r2rml.exception.InvalidR2RMLSyntaxException;
import net.antidot.semantic.rdf.rdb2rdf.r2rml.exception.R2RMLDataError;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFParseException;

/**
 *
 * @author callocca
 */
public class CustomRMLMappingFile {
    
    private String reverseRMLpropertiesFile="";
    
    public CustomRMLMappingFile(){
        super();
    }

    public CustomRMLMappingFile(String reverseRMLpropertiesFilePath){
        this.reverseRMLpropertiesFile=reverseRMLpropertiesFilePath;
    }

    
    public boolean isRMLMappingFileTree(String path2RMLFile){
        
        //System.out.println("Checking whether the RML Mapping File is a Tree or a Forest...");
        
        try {
            //1. Parsing the rml mapping file
            RMLMapping mapping = RMLMappingFactory.extractRMLMapping(path2RMLFile);
            
            //2. Collecting all the triple maps
            Collection<TriplesMap> tripleMapSet = (Collection<TriplesMap>) mapping.getTriplesMaps();
            
            //3. Identify the ROOT TripleMap  
            ArrayList<TriplesMap> rootTripleMaps = identifyROOTTripleMap(tripleMapSet);
            
            if(rootTripleMaps.isEmpty()){
               //System.out.println("It seems that there are not root nodes");
                return false;
            }
            if(rootTripleMaps.size()>1){
                //System.out.println("The RML Mapping File forms a forest: it is not fully reversable.");
                return false;
            }
            if(rootTripleMaps.size()==1){
                //System.out.println("The RML Mapping File forms a tree: condition 1 holds.");
                return true;
            }
            
        } catch (InvalidR2RMLStructureException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidR2RMLSyntaxException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (R2RMLDataError ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RepositoryException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RDFParseException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return false;
    }
    
    public boolean isRMLMappingFileForest(String path2RMLFile){
        
        //System.out.println("Checking whether the RML Mapping File is a Tree or a Forest...");
        
        try {
            //1. Parsing the rml mapping file
            RMLMapping mapping = RMLMappingFactory.extractRMLMapping(path2RMLFile);
            
            //2. Collecting all the triple maps
            Collection<TriplesMap> tripleMapSet = (Collection<TriplesMap>) mapping.getTriplesMaps();
            
            //3. Identify the ROOT TripleMap  
            ArrayList<TriplesMap> rootTripleMaps = identifyROOTTripleMap(tripleMapSet);
            
            if(rootTripleMaps.isEmpty()){
                //System.out.println("It seems that there are not root nodes");
                return false;
            }
            if(rootTripleMaps.size()>1){
                //System.out.println("The RML Mapping File forms a forest: it is not fully reversable.");
                return true;
            }
            if(rootTripleMaps.size()==1){
                //System.out.println("The RML Mapping File forms a tree: condition 1 holds.");
                return false;
            }
            
        } catch (InvalidR2RMLStructureException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidR2RMLSyntaxException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (R2RMLDataError ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RepositoryException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RDFParseException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return false;
    }
    
    public ArrayList<TriplesMap> identifyTheRootsTripleMap(String path2RMLFile){
        
        ArrayList<TriplesMap> rootTripleMaps = new ArrayList<TriplesMap>();
        try {
            //1. Parsing the rml mapping file
            RMLMapping mapping = RMLMappingFactory.extractRMLMapping(path2RMLFile);
            
            //2. Collecting all the triple maps
            Collection<TriplesMap> tripleMapSet = (Collection<TriplesMap>) mapping.getTriplesMaps();
            
            //3. Identify the ROOT TripleMap  
            rootTripleMaps = identifyROOTTripleMap(tripleMapSet);
            
            return rootTripleMaps;
            
        } catch (InvalidR2RMLStructureException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidR2RMLSyntaxException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (R2RMLDataError ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RepositoryException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RDFParseException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return null;
    }
    
    public TriplesMap identifyTheRootTripleMap(String path2RMLFile){
        
        ArrayList<TriplesMap> rootTripleMaps = new ArrayList<TriplesMap>();
        try {
            //1. Parsing the rml mapping file
            RMLMapping mapping = RMLMappingFactory.extractRMLMapping(path2RMLFile);
            
            //2. Collecting all the triple maps
            Collection<TriplesMap> tripleMapSet = (Collection<TriplesMap>) mapping.getTriplesMaps();
            
            //3. Identify the ROOT TripleMap  
            rootTripleMaps = identifyROOTTripleMap(tripleMapSet);
            
            if(!(rootTripleMaps.isEmpty())){
                return rootTripleMaps.get(0);
            }
        } catch (InvalidR2RMLStructureException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidR2RMLSyntaxException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (R2RMLDataError ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RepositoryException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RDFParseException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
        
    }
   
    public Collection<TriplesMap> getTriplesMapCollection(String path2RMLFile){
        try {
            //1. Parsing the rml mapping file
            RMLMapping mapping = RMLMappingFactory.extractRMLMapping(path2RMLFile);
            
            //2. Collecting all the triple maps
            Collection<TriplesMap> tripleMapSet = (Collection<TriplesMap>) mapping.getTriplesMaps();
            
            return tripleMapSet;
            
        } catch (InvalidR2RMLStructureException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidR2RMLSyntaxException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (R2RMLDataError ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RepositoryException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RDFParseException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public boolean isStructuralEnriched(String path2RMLFile){
        
        String rowNumberTripleMap="";
        String rowObjectProperty="";
        //boolean isDAG=false;
        if(!"".equals(this.reverseRMLpropertiesFile)){
            ReverseRMLPropertiesFile reverseProperty;
            reverseProperty = new ReverseRMLPropertiesFile(this.reverseRMLpropertiesFile);
            rowNumberTripleMap=reverseProperty.getValue("rowIDTripleMap");
            rowObjectProperty=reverseProperty.getValue("rowStructuralProperty");
            //System.out.println("rowNumberTripleMap value is " +rowNumberTripleMap);
            //System.out.println("rowObjectProperty value is  " +rowObjectProperty);
            if(!("".equals(rowNumberTripleMap)) && !("".equals(rowObjectProperty))){
                if(rowNumberTripleMap==null || rowObjectProperty==null){
                    System.out.println("[CustomRMLMappingFile]: isStructuralEnrichedNew, the structural info is null ");
                    return false;
                }
                //3.1 checking each triple map has an predicateObjectMap with :predicate = rowObjectProperty
                //    and which, in turn, has an parentTriplesMap = rowNumberTripleMap.
                try {
                    //1. Parsing the rml mapping file
                    RMLMapping mapping = RMLMappingFactory.extractRMLMapping(path2RMLFile);
                    //2. Collecting all the triple maps
                    Collection<TriplesMap> tripleMapSet = (Collection<TriplesMap>) mapping.getTriplesMaps();
                    if(!tripleMapSet.isEmpty()){
                        //System.out.println("It seems that there are not root nodes");
                        //We need to check whether all the tripleMap has got the structural enrichemnt. 
                        boolean isStructuralEnriched=false;
                    
                        // for each tripleMap
                        for(TriplesMap tm:tripleMapSet){
                            System.out.println("[CustomRMLMappingFile]: isStructuralEnrichedNew, current TripleMap "+tm.getName());
                            //if it is different from the structural one
                            if(!(tm.getName().equals(rowNumberTripleMap))){
                                Set<PredicateObjectMap> predObjMapSet = tm.getPredicateObjectMaps();
                                //and it does not have any predicate object map, return false.
                                if(predObjMapSet.isEmpty()){
                                    return false;
                                }
                                //otherwise
                                isStructuralEnriched=false;
                                for(PredicateObjectMap predObjMap:predObjMapSet){
                                    //check the only one that has got a ReferencingObjectMap
                                    if(predObjMap.hasReferencingObjectMaps()){
                                        //for those cases,  
                                        Set<PredicateMap> predObj=predObjMap.getPredicateMaps();
                                        for(PredicateMap p:predObj){
                                            System.out.println("[CustomRMLMappingFile]: isStructuralEnrichedNew, Predicate " +p.getConstantValue().stringValue());
                                            if(p.getConstantValue().stringValue().equals(rowObjectProperty)){
                                                Set<ReferencingObjectMap> r_o_ms=p.getPredicateObjectMap().getReferencingObjectMaps();
                                                for(ReferencingObjectMap rom:r_o_ms){
                                                    if(rom.getParentTriplesMap().getName().equals(rowNumberTripleMap)){
                                                        isStructuralEnriched=true;
                                                        break ;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } 
                        }
                        return isStructuralEnriched;
                    }
                    else{
                        System.out.println("[CustomRMLMappingFile]: isStructuralEnrichedNew: There are not TripleMaps defined in the rml mapping file. ");
                        return false;
                    }
                } catch (InvalidR2RMLStructureException ex) {
                    Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidR2RMLSyntaxException ex) {
                    Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
                } catch (R2RMLDataError ex) {
                    Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
                } catch (RepositoryException ex) {
                    Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
                } catch (RDFParseException ex) {
                    Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            System.out.println("[CustomRMLMappingFile]: isStructuralEnrichedNew: There is not a structuralProperty or a StructuralTripleMap set. ");
            return false;
        }
        else{
            System.out.println("[CustomRMLMappingFile]: isStructuralEnrichedNew: No reverseRMLMapping properties file provided. ");
            return false;
        }        
    }
    
    
    
    private ArrayList<TriplesMap> identifyROOTTripleMap(Collection<TriplesMap> tripleMapCollection){
        
        //3.1 check if the triplemap is the " If a Triples Map is an object of a triple, it is a referencing object map, thus can't be root."
        ArrayList<TriplesMap> listOfRootNodes=new ArrayList<TriplesMap>();
        
        for(TriplesMap triple_map:tripleMapCollection){ //for each elem in the tripleMapCollection
            
            //controlla se elem is an ReferencingObjectMap in the tripleMapCollection
            boolean isReferencingObjectMap =false;
            
            String TMname=triple_map.getName();    
            for(TriplesMap node:tripleMapCollection){
                String tmName=node.getName();
                if(!(TMname.equals(tmName))){
                    Set<PredicateObjectMap> predObjMapSet= node.getPredicateObjectMaps();
                    for(PredicateObjectMap predObjMap:predObjMapSet){
                       if(predObjMap.hasReferencingObjectMaps()){
                            Set<ReferencingObjectMap> r_o_ms=predObjMap.getReferencingObjectMaps();
                            for(ReferencingObjectMap rom:r_o_ms){
                                if(rom.getParentTriplesMap().getName().equals(TMname)){
                                    isReferencingObjectMap=true;
                                }
                            }            
                       }
                    }    
                }            
            }
            if(isReferencingObjectMap==false){
                listOfRootNodes.add(triple_map);
            }
        }  
        return listOfRootNodes;
        
    }
    
    // this method checks whether each triple map has got the structural enrichement as described in the paper. 
//    public boolean isStructuralEnriched(String path2RMLFile){
//        
//        String rowNumberTripleMap;
//        String rowObjectProperty;
//        //boolean isDAG=false;
//        if(!"".equals(reverseRMLpropertiesFile)){
//            ReverseRMLPropertiesFile reverseProperty;
//            reverseProperty = new ReverseRMLPropertiesFile(reverseRMLpropertiesFile);
//            
//            rowNumberTripleMap=reverseProperty.getValue("rowNumberTripleMap");
//            rowObjectProperty=reverseProperty.getValue("rowObjectProperty");
//            
//            System.out.println("rowNumberTripleMap value is " +rowNumberTripleMap);
//            System.out.println("rowObjectProperty value is  " +rowObjectProperty);
//            //3.1 checking each triple map has an predicateObjectMap with :predicate = rowObjectProperty
//            //    and which, in turn, has an parentTriplesMap = rowNumberTripleMap.
//            try {
//                //1. Parsing the rml mapping file
//                RMLMapping mapping = RMLMappingFactory.extractRMLMapping(path2RMLFile);
//            
//                //2. Collecting all the triple maps
//                Collection<TriplesMap> tripleMapSet = (Collection<TriplesMap>) mapping.getTriplesMaps();
//                
//                if(!(tripleMapSet.isEmpty())){
//                    //System.out.println("It seems that there are not root nodes");
//                    //We need to check whether all the tripleMap has got the structural enrichemnt. 
//                    boolean isStructuralEnriched=true;
//                    for(TriplesMap tm:tripleMapSet){
//                        if(!tm.getName().equals(rowNumberTripleMap)){
//                            Set<PredicateObjectMap> predObjMapSet = tm.getPredicateObjectMaps();
//                            boolean checkingPOB=false;
//                            for(PredicateObjectMap predObjMap:predObjMapSet){
//                                boolean checkingRefObjMap=false;
//                                if(predObjMap.hasReferencingObjectMaps()){
//                                    Set<ReferencingObjectMap> r_o_ms=predObjMap.getReferencingObjectMaps();
//                                    for(ReferencingObjectMap rom:r_o_ms){
//                                        if(rom.getParentTriplesMap().getName().equals(rowNumberTripleMap)){
//                                            checkingRefObjMap=true;
//                                            //break ;
//                                        }
//                                    }
//                                    if(checkingRefObjMap){
//                                        checkingPOB=true;
//                                    }
//                                }
//                            }
//                            if(checkingPOB){
//                                isStructuralEnriched=false;
//                            }
//                        } 
//                    }
//                    return isStructuralEnriched;
//                }
//            } catch (InvalidR2RMLStructureException ex) {
//                Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (InvalidR2RMLSyntaxException ex) {
//                Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (R2RMLDataError ex) {
//                Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (RepositoryException ex) {
//                Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (RDFParseException ex) {
//                Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (IOException ex) {
//                Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//            
//        return false;
//        
//    }
//    
      // this method checks whether each triple map has got the structural enrichement as described in the paper. 

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // we don't need this anymore when the structural Triple Map is applied consistently.
    public boolean isSingleDAG(String path2RMLFile){
        
        //1. check if it has only one root node
        // it just check it the EML mapping file has one root or more than one.
        boolean isSingleDG=isRMLMappingFileSingleDG(path2RMLFile);
        
        if(isSingleDG){
            //2. check if the set of Triple Maps has any "strongly connected components"
            // TO DO
            //3. Make sure that the special node has (number of TripleMap -1) entranti archi.
            String rowNumberTripleMap;
            String rowObjectProperty;
            //boolean isDAG=false;
            if(!"".equals(reverseRMLpropertiesFile)){
                ReverseRMLPropertiesFile reverseProperty;
                reverseProperty = new ReverseRMLPropertiesFile(reverseRMLpropertiesFile);
            
                rowNumberTripleMap=reverseProperty.getValue("rowNumberTripleMap");
                rowObjectProperty=reverseProperty.getValue("rowObjectProperty");
            
                System.out.println("rowNumberTripleMap value is " +rowNumberTripleMap);
                System.out.println("rowObjectProperty value is  " +rowObjectProperty);
                //3.1 checking each triple map has an predicateObjectMap with :predicate = rowObjectProperty
                //    and which, in turn, has an parentTriplesMap = rowNumberTripleMap.
                
                return true;
            }
            return false;
        }
        return false;
    }
    // we don't need this anymore when the structural Triple Map is applied consistently.
    private boolean isRMLMappingFileSingleDG(String path2RMLFile){
        
        //System.out.println("Checking whether the RML Mapping File is a Tree or a Forest...");
        
        try {
            //1. Parsing the rml mapping file
            RMLMapping mapping = RMLMappingFactory.extractRMLMapping(path2RMLFile);
            
            //2. Collecting all the triple maps
            Collection<TriplesMap> tripleMapSet = (Collection<TriplesMap>) mapping.getTriplesMaps();
            
            //3. Identify the ROOT TripleMap  
            ArrayList<TriplesMap> rootTripleMaps = identifyROOTTripleMap(tripleMapSet);
            
            if(rootTripleMaps.isEmpty()){
               //System.out.println("It seems that there are not root nodes");
                return false;
            }
            if(rootTripleMaps.size()>1){
                //System.out.println("The RML Mapping File forms a forest: it is not fully reversable.");
                return false;
            }
            if(rootTripleMaps.size()==1){
                //System.out.println("The RML Mapping File forms a tree: condition 1 holds.");
                return true;
            }
            
        } catch (InvalidR2RMLStructureException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidR2RMLSyntaxException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (R2RMLDataError ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RepositoryException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RDFParseException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CustomRMLMappingFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return false;
    }
    
   
    
}
