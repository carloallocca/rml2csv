/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.hcmr.imbbc.rmlreverse.project.main;

import be.ugent.mmlab.rml.model.TriplesMap;
import gr.hcmr.imbbc.rmlreverse.project.tools.CustomRMLMappingFile;

import java.util.ArrayList;

/**
 *
 * @author callocca
 */
public class TestingCustomRMLMappingFile {
 
     public static void main(String[] args){
        
        System.out.println("Start");
        
        String path2RMLFile ="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLProcessor\\ReverseRMLInput\\RMLmappingFileForBuildingDependencyTree.rml.ttl";
        //String path2RMLFile ="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLProcessor\\ReverseRMLInput\\RMLmappingFileForBuildingForest.rml.ttl";
        
        CustomRMLMappingFile rmlMappingFile= new CustomRMLMappingFile();
        
        boolean tree=rmlMappingFile.isRMLMappingFileTree(path2RMLFile);
        boolean forest=rmlMappingFile.isRMLMappingFileForest(path2RMLFile);//.isRMLMappingFileTree(path2RMLFile);
        
        if(tree){
            System.out.println("[TestingCustomRMLMappingFile:Main] The RML Mapping File is a Tree");
            TriplesMap tm=rmlMappingFile.identifyTheRootTripleMap(path2RMLFile);
            if(!(tm==null)){
                System.out.println("[TestingCustomRMLMappingFile:Main] The root TripleMap is " +tm.getName());
            }
        }
        if(forest){
            System.out.println("[TestingCustomRMLMappingFile:Main] The RML Mapping File is a Forest");
            ArrayList<TriplesMap> tmForest=rmlMappingFile.identifyTheRootsTripleMap(path2RMLFile);
            if(!(tmForest==null)){
                for(TriplesMap tm1:tmForest){
                    System.out.println("[TestingCustomRMLMappingFile:Main] A root of the Forest is " +tm1.getName());
                }
            }
        } 
                
        
        System.out.println("End");
        
        
        
    }
}
