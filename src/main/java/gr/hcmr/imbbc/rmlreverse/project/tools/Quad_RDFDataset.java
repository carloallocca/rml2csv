/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.hcmr.imbbc.rmlreverse.project.tools;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.tdb.TDBLoader;
import com.hp.hpl.jena.tdb.base.file.Location;
import com.hp.hpl.jena.tdb.sys.TDBInternal;
import com.hp.hpl.jena.util.FileManager;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author callocca
 */
public class Quad_RDFDataset implements RDFDataset{

    private String RDFDatasetPath="";
    private String TDBdir="";
    
    private Dataset RDFDataset;
    
    public Quad_RDFDataset(){
        super();
    }
    
    public Quad_RDFDataset(String rdfDatasetPath, String tdbDir){
        this.RDFDatasetPath=rdfDatasetPath;
        this.TDBdir=tdbDir;
        
        FileManager fm = FileManager.get();
        fm.addLocatorClassLoader(Quad_RDFDataset.class.getClassLoader());
        InputStream in = fm.open(this.RDFDatasetPath);
        Location location = new Location (this.TDBdir);
        // Load some initial data
        TDBLoader.load(TDBInternal.getBaseDatasetGraphTDB(TDBFactory.createDatasetGraph(location)), in, false);
        this.RDFDataset = TDBFactory.createDataset(location);
        
    }
    
    

    
    
    @Override
    public ArrayList<String> selectDistinctSubject(String fullyClassURI) {
        
       //this is the query used when I load a quad file to select the distinct subjects
        String queryString =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT distinct ?subject ?g "+
                "{"
                    +"{ GRAPH ?g "
                        +" {?subject rdf:type <"+fullyClassURI+"> .}"
                    +"}"
                +"}"; 
        
        RDFDataset.begin(ReadWrite.READ);
        try {
                ArrayList<String> distinctSubject = new ArrayList();
                Query query = QueryFactory.create(queryString); //s2 = the query above
                QueryExecution qExe = QueryExecutionFactory.create(query, RDFDataset);
                ResultSet resultsRes = qExe.execSelect();
                              
             // Output query results   
                List<QuerySolution> solList=ResultSetFormatter.toList(resultsRes);
                if(!(solList==null)){
                    for(QuerySolution row:solList ){
                        distinctSubject.add(row.toString());
                    }
                    qExe.close(); 
                    return distinctSubject;
                }
                else{
                    qExe.close(); 
                    return null;
                }
                        //ResultSetFormatter.out(System.out, resultsRes, query);
                
            } finally {
                RDFDataset.end();
            }
        
        
    }

    @Override
    public ArrayList<String> selectDistinctObject(String subjectURI, String predicateURI, String rdfTypeClassURI) {
                // Create the query
        String queryString =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
              "SELECT distinct ?s ?g "+
                "{"
                    +"{ GRAPH ?g "
                        +"{"
                            +"<"+subjectURI+">"+ "<"+predicateURI+">"+ " ?object . "
                            +"?object rdf:type"+ "<"+rdfTypeClassURI+"> ."
                        + "}"
                    +"}"
                +"}";    
                
            
        RDFDataset.begin(ReadWrite.READ);
        try {
                ArrayList<String> distinctSubject = new ArrayList();
                Query query = QueryFactory.create(queryString); //s2 = the query above
                QueryExecution qExe = QueryExecutionFactory.create(query, RDFDataset);
                ResultSet resultsRes = qExe.execSelect();
             // Output query results   
             //   ResultSetFormatter.out(System.out, resultsRes, query);

                
                                List<QuerySolution> solList=ResultSetFormatter.toList(resultsRes);
                if(!(solList==null)){
                    for(QuerySolution row:solList ){
                        distinctSubject.add(row.get("object").toString());
                    }
                    qExe.close(); 
                    return distinctSubject;
                }
                else{
                    qExe.close(); 
                    return null;
                }
            } finally {
                RDFDataset.end();
            }

    }

    @Override
    public ArrayList<QuerySolution> executeSelectQuery(String queryString) {
         // Create the query
        RDFDataset.begin(ReadWrite.READ);
        try {
              
                Query query = QueryFactory.create(queryString); //s2 = the query above
                QueryExecution qExe = QueryExecutionFactory.create(query, RDFDataset);
                ResultSet resultsRes = qExe.execSelect();
                // Output query results   
                //ResultSetFormatter.out(System.out, resultsRes, query);
                ArrayList<QuerySolution> solList=(ArrayList<QuerySolution>) ResultSetFormatter.toList(resultsRes);
                if(!(solList==null)){
                
                    qExe.close();
                    return solList;
                }
                else{
                    qExe.close(); 
                    return null;
                }
                        //ResultSetFormatter.out(System.out, resultsRes, query);
                
            } finally {
                RDFDataset.end();
            }
    }
    
}
