/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.hcmr.imbbc.rmlreverse.project.main;

import be.ugent.mmlab.rml.model.TriplesMap;
import gr.hcmr.imbbc.rmlreverse.project.strategy.CSVReverseRMLStrategy;
import gr.hcmr.imbbc.rmlreverse.project.strategy.ReverseRMLStrategy;
import gr.hcmr.imbbc.rmlreverse.project.tools.CustomRMLMappingFile;
import java.util.ArrayList;

/**
 *
 * @author callocca
 */
public class TestingCSVReverseRMLStrategy {
    public static void main(String[] args){
        
        String dataset_id = "6";
        
        String path2RMLFile ="E:\\RMLData\\InputDataset\\dataset"+dataset_id+"\\rml"+dataset_id+".ttl";
        String path2RDFDataset ="E:\\RMLData\\InputDataset\\dataset"+dataset_id+"\\rdf"+dataset_id+".nt";
        String path2CSVDataSource ="E:\\RMLData\\InputDataset\\dataset"+dataset_id+"\\dataset"+dataset_id+".csv";        
        String tdbDir="E:\\RMLData\\InputDataset\\dataset"+dataset_id+"\\tmp";
    
        ReverseRMLStrategy rmlMappingFile= new CSVReverseRMLStrategy(path2RMLFile,path2RDFDataset,path2CSVDataSource, tdbDir);
        
        rmlMappingFile.reverseRML();
        
        System.out.println("End");
    }
}
