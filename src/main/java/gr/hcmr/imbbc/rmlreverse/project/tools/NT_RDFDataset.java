/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.hcmr.imbbc.rmlreverse.project.tools;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.tdb.TDBLoader;
import com.hp.hpl.jena.tdb.base.file.Location;

import com.hp.hpl.jena.tdb.sys.TDBInternal;
import com.hp.hpl.jena.util.FileManager;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author callocca
 */
public class NT_RDFDataset implements RDFDataset{
 
    private String RDFDatasetPath="";
    private String TDBdir="";
    
    private Dataset RDFDataset;
    
    public NT_RDFDataset(){
        super();
    }
    
    public NT_RDFDataset(String rdfDatasetPath, String tdbDir){
        this.RDFDatasetPath=rdfDatasetPath;
        this.TDBdir=tdbDir;
        FileManager fm = FileManager.get();
        fm.addLocatorClassLoader(NT_RDFDataset.class.getClassLoader());
        InputStream in = fm.open(this.RDFDatasetPath);
        System.out.println("[NT_RDFDataset] NT_RDFDataset  "+this.TDBdir);
        Location location = new Location (this.TDBdir);
        // Load some initial data
        TDBLoader.load(TDBInternal.getBaseDatasetGraphTDB(TDBFactory.createDatasetGraph(location)), in, false);
        //TDBLoader.load(TDBInternal.getBaseDatasetGraphTDB(TDBFactory.createDatasetGraph(this.TDBdir)), in, false);
        this.RDFDataset = TDBFactory.createDataset(location);
        //  this.RDFDataset = TDBFactory.createDataset(this.TDBdir);
        
    }
    
    
   
    
    @Override
    public ArrayList<String> selectDistinctSubject(String fullyClassURI){
        System.out.println("[NT_RDFDataset],selectDistinctSubject "+fullyClassURI);
        // Create the query
        String queryString =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT distinct ?s "+
                "WHERE "
                +" { ?s rdf:type <"+fullyClassURI+">. }";
        
        RDFDataset.begin(ReadWrite.READ);
        try {
                ArrayList<String> distinctSubject = new ArrayList();
                Query query = QueryFactory.create(queryString); //s2 = the query above
                QueryExecution qExe = QueryExecutionFactory.create(query, RDFDataset);
                ResultSet resultsRes = qExe.execSelect();
             // Output query results   
                List<QuerySolution> solList=ResultSetFormatter.toList(resultsRes);
                if(!(solList==null)){
                    for(QuerySolution row:solList ){
                        distinctSubject.add(row.get("s").toString());
                    }
                    qExe.close(); 
                    return distinctSubject;
                }
                else{
                    qExe.close(); 
                    return null;
                }
                        //ResultSetFormatter.out(System.out, resultsRes, query);
                
            } finally {
                RDFDataset.end();
            }
        //return    null; 
    }
    
    
    
    @Override
    public ArrayList<String> selectDistinctObject(String subjectURI, String predicateURI, String rdfTypeClassURI) {
        // Create the query
        String queryString =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT distinct ?object "+
                "WHERE "
                    +" {"
                        +"<"+subjectURI+">"+ "<"+predicateURI+">"+ " ?object . "
                        +"?object rdf:type"+ "<"+rdfTypeClassURI+"> ."
                    + "}";
        
        RDFDataset.begin(ReadWrite.READ);
        try {
                ArrayList<String> distinctSubject = new ArrayList();
                Query query = QueryFactory.create(queryString); //s2 = the query above
                QueryExecution qExe = QueryExecutionFactory.create(query, RDFDataset);
                ResultSet resultsRes = qExe.execSelect();
             // Output query results   
             //   ResultSetFormatter.out(System.out, resultsRes, query);

                
                List<QuerySolution> solList=ResultSetFormatter.toList(resultsRes);
                
                if(!(solList==null)){
                    for(QuerySolution row:solList ){
                        distinctSubject.add(row.get("object").toString());
                    }
                qExe.close(); 
                return distinctSubject;
                }

                
                
//                if(!(resultsRes.getRowNumber()==0)){
//                    while (resultsRes.hasNext()) {
//                        QuerySolution row=resultsRes.nextSolution();
//                        distinctSubject.add(row.get("object").toString());
//                //        System.out.println("row: " + row.get("object").toString());               
//                    }
//                qExe.close(); 
//                return distinctSubject;
//                }
                else{
                qExe.close(); 
                return null;
                }
                        //ResultSetFormatter.out(System.out, resultsRes, query);
                
            } finally {
                RDFDataset.end();
            }
    }

    @Override
    public ArrayList<QuerySolution> executeSelectQuery(String queryString) {
        // Create the query
        RDFDataset.begin(ReadWrite.READ);
        try {
              
                Query query = QueryFactory.create(queryString); //s2 = the query above
                QueryExecution qExe = QueryExecutionFactory.create(query, RDFDataset);
                ResultSet resultsRes = qExe.execSelect();
                // Output query results   
                //ResultSetFormatter.out(System.out, resultsRes, query);
                ArrayList<QuerySolution> solList=(ArrayList<QuerySolution>) ResultSetFormatter.toList(resultsRes);
                if(!(solList==null)){
                
                    qExe.close();
                    return solList;
                }
                else{
                    qExe.close(); 
                    return null;
                }
                        //ResultSetFormatter.out(System.out, resultsRes, query);
                
            } finally {
                RDFDataset.end();
            }
    }
    
    
//    public ArrayList<String> selectDistinctSubjectNew(String fullyClassURI){
//        
//        // Create the query
//        String queryString =        
//            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
//            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
//            "SELECT  ?s "+
//                "WHERE "
//                +" { ?s rdf:type <"+fullyClassURI+">. }";
//        
//        
//        FileManager fm = FileManager.get();
//        fm.addLocatorClassLoader(NT_RDFDataset.class.getClassLoader());
//        InputStream in = fm.open(this.RDFDatasetPath);
//        Location location = new Location (this.TDBdir);
//        // Load some initial data
//        TDBLoader.load(TDBInternal.getBaseDatasetGraphTDB(TDBFactory.createDatasetGraph(location)), in, false);
//        this.RDFDataset = TDBFactory.createDataset(location);
//        
//        this.RDFDataset.begin(ReadWrite.READ);
//        try {
//                ArrayList<String> distinctSubject = new ArrayList();
//                Query query = QueryFactory.create(queryString); //s2 = the query above
//                QueryExecution qExe = QueryExecutionFactory.create(query, this.RDFDataset);
//                ResultSet resultsRes = qExe.execSelect();
//             // Output query results   
//                ResultSetFormatter.out(System.out, resultsRes, query);
//            
//                if(!(resultsRes.getRowNumber()==0)){
//                    while (resultsRes.hasNext()) {
//                        QuerySolution row=resultsRes.nextSolution();
//                        distinctSubject.add(row.get("s").toString());
//                        System.out.println("row: " + row.get("s").toString());               
//                    }
//                qExe.close(); 
//                return distinctSubject;
//                }
//                else{
//                qExe.close(); 
//                return null;
//                }
//                        //ResultSetFormatter.out(System.out, resultsRes, query);
//                
//            } finally {
//                this.RDFDataset.end();
//            }
//        //return    null; 
//    }
//    
    
     


    public void quadModel5(){
        //String quadFilePath="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLReverseQuadModel\\Input\\rdf1.nq";
        //String TDBdir="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLReverseQuadModel\\DatasetDir";
        
        
        String quadFilePath="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLReverseQuadModel\\Input\\rdfdataset_with_prov.nq";
        String TDBdir="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLReverseQuadModel\\DatasetDir";
        
        
        FileManager fm = FileManager.get();
        
        fm.addLocatorClassLoader(NT_RDFDataset.class.getClassLoader());
        InputStream in = fm.open(quadFilePath);

        Location location = new Location (TDBdir);

        // Load some initial data
        TDBLoader.load(TDBInternal.getBaseDatasetGraphTDB(TDBFactory.createDatasetGraph(location)), in, false);
        
        // Create a new query
        String queryString =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT ?s ?p ?o ?g ?mappingFile ?mappingRule"+
                "WHERE { "
                +" { ?s ?p ?o } UNION " 
                +    "{ GRAPH ?g "
                +       "{ ?s ?p ?o."
                           
                    + " } "
                + "}"
                + " }";
                // Create a new query
        String queryString1 =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT distinct ?s ?p ?o ?mappingFile ?mappingRule"+
                " { "
                    +   "  ?g <http://www.w3.org/ns/prov#Used>  ?rml_mapping_file."
                    +   "  ?rml_mapping_file rdfs:label ?mappingFile. "
                    +   "  ?g <http://www.w3.org/ns/prov#Used>  ?rml_mapping_specification."
                    +   "  ?rml_mapping_specification rdfs:label ?mappingRule. "
                    +"{ GRAPH ?g "
                +       "{ ?s ?p ?o."
                                        + " } "
                + "}"
                + " }";
         String queryString2 =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT  ?s ?p ?o ?mappingFile ?mappingRule"+
                " { "
                    +   "  ?g <http://www.w3.org/ns/prov#Used>  ?rml_mapping_file."
                    +   "  ?rml_mapping_file rdfs:label ?mappingFile. "
                    +"{ GRAPH ?g "
                    +       "{ ?s ?p ?o."+ " } "
                    +   "  ?g <http://www.w3.org/ns/prov#Used>  ?rml_mapping_specification."
                    +   "  ?rml_mapping_specification rdfs:label ?mappingRule. "
                    + "}"
                + " }";
        
        //this is the query used when i load a quad file
        String queryString3 =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT  ?s ?p ?o ?mappingFile ?mappingRule"+
                " { "
                    +   "  ?g <http://www.w3.org/ns/prov#Used1>  ?rml_mapping_file."
                    +   "  ?rml_mapping_file rdfs:label ?mappingFile. "
                    +   "  ?g <http://www.w3.org/ns/prov#Used2>  ?rml_mapping_specification."
                    +   "  ?rml_mapping_specification rdfs:label ?mappingRule. "
                    +"{ GRAPH ?g "
                    +       "{ ?s ?p ?o."+ " } "
                    
                    + "}"
                + " }";
        
        //this is the query used when I load a quad file to select the distinct subjects
        String queryString101 =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT distinct ?s ?g "+
                " { "
                    +"{ GRAPH ?g "
                    +       "{ ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.ics.forth.gr/isl/MarineTLO/v4/marinetlo.owl/BC21_Dataset>."+ " } "
                    
                    + "}"
                + " }"; 
         
         // Create the query
        String queryString100 =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT  ?s "+
                "WHERE "
                +" {?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.ics.forth.gr/isl/MarineTLO/v4/marinetlo.owl/BC21_Dataset>. }";
         
        Dataset dataset = TDBFactory.createDataset(location);
        dataset.begin(ReadWrite.READ);
        try {
            Query query = QueryFactory.create(queryString101); //s2 = the query above
            QueryExecution qExe = QueryExecutionFactory.create(query, dataset);
            ResultSet resultsRes = qExe.execSelect();
             // Output query results    
            ResultSetFormatter.out(System.out, resultsRes, query);
            qExe.close(); 
        
        } finally {
            dataset.end();
        }
        
    }
    
    public void quadModel4(){
        //String quadFilePath="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLReverseQuadModel\\Input\\rdf1.nq";
        //String TDBdir="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLReverseQuadModel\\DatasetDir";
        
        
        String quadFilePath="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLReverseQuadModel\\Input\\rdfdataset_with_prov.nq";
        String TDBdir="C:\\Users\\callocca\\Desktop\\HCMR Fellow\\HCMR Fellow\\HCMR Development\\RMLReverseQuadModel\\DatasetDir";
        
        
        FileManager fm = FileManager.get();
        
        fm.addLocatorClassLoader(NT_RDFDataset.class.getClassLoader());
        InputStream in = fm.open(quadFilePath);

        Location location = new Location (TDBdir);

        // Load some initial data
        TDBLoader.load(TDBInternal.getBaseDatasetGraphTDB(TDBFactory.createDatasetGraph(location)), in, false);
        
        // Create a new query
        String queryString =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT ?s ?p ?o ?g ?mappingFile ?mappingRule"+
                "WHERE { "
                +" { ?s ?p ?o } UNION " 
                +    "{ GRAPH ?g "
                +       "{ ?s ?p ?o."
                           
                    + " } "
                + "}"
                + " }";
                // Create a new query
        String queryString1 =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT distinct ?s ?p ?o ?mappingFile ?mappingRule"+
                " { "
                    +   "  ?g <http://www.w3.org/ns/prov#Used>  ?rml_mapping_file."
                    +   "  ?rml_mapping_file rdfs:label ?mappingFile. "
                    +   "  ?g <http://www.w3.org/ns/prov#Used>  ?rml_mapping_specification."
                    +   "  ?rml_mapping_specification rdfs:label ?mappingRule. "
                    +"{ GRAPH ?g "
                +       "{ ?s ?p ?o."
                                        + " } "
                + "}"
                + " }";
         String queryString2 =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT  ?s ?p ?o ?mappingFile ?mappingRule"+
                " { "
                    +   "  ?g <http://www.w3.org/ns/prov#Used>  ?rml_mapping_file."
                    +   "  ?rml_mapping_file rdfs:label ?mappingFile. "
                    +"{ GRAPH ?g "
                    +       "{ ?s ?p ?o."+ " } "
                    +   "  ?g <http://www.w3.org/ns/prov#Used>  ?rml_mapping_specification."
                    +   "  ?rml_mapping_specification rdfs:label ?mappingRule. "
                    + "}"
                + " }";
        
         String queryString3 =        
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
            "SELECT  ?s ?p ?o ?mappingFile ?mappingRule"+
                " { "
                    +   "  ?g <http://www.w3.org/ns/prov#Used1>  ?rml_mapping_file."
                    +   "  ?rml_mapping_file rdfs:label ?mappingFile. "
                    +   "  ?g <http://www.w3.org/ns/prov#Used2>  ?rml_mapping_specification."
                    +   "  ?rml_mapping_specification rdfs:label ?mappingRule. "
                    +"{ GRAPH ?g "
                    +       "{ ?s ?p ?o."+ " } "
                    
                    + "}"
                + " }";
         
        Dataset dataset = TDBFactory.createDataset(location);
        dataset.begin(ReadWrite.READ);
        try {
            Query query = QueryFactory.create(queryString3); //s2 = the query above
            QueryExecution qExe = QueryExecutionFactory.create(query, dataset);
            ResultSet resultsRes = qExe.execSelect();
             // Output query results    
            ResultSetFormatter.out(System.out, resultsRes, query);
            qExe.close(); 
        
        } finally {
            dataset.end();
        }
        
    }
    
}
